const cacheFunction = require("../cacheFunction.cjs")

let cb = (...args) => {
    // console.log(args)
    // let args_array = JSON.parse(args)
    // console.log(args_array)
    return args.reduce((acc, current) => acc + current, 0)
}
try {
    let result = cacheFunction(cb)

    console.log(result(1, 2, 3))
    console.log(result(1, 2, 3, 4))
    console.log(result(1, 2, 3))
} catch (error) {
    console.log(error)
}

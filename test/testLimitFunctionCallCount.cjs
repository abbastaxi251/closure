const limitFunctionCallCount = require("../limitFunctionCallCount.cjs")

const cb = (...args) => {
    // console.log(a,b,c)
    return "invoking cb " + args
}
try {
    let invoke_cb = limitFunctionCallCount(cb,3)
    console.log(invoke_cb(2,3))
    // console.log(invoke_cb([0,1],'hello'))
    console.log(invoke_cb(2,3,4))
    console.log(invoke_cb())
    console.log(invoke_cb())
} catch (error) {
    console.log(error)
}

const counterFactory = require("../counterFactory.cjs")

let count = counterFactory()

count.increment()
count.increment()
console.log(count.increment())

function counterFactory() {
    // Return an object that has two methods called `increment` and `decrement`.
    // `increment` should increment a counter variable in closure scope and return it.
    // `decrement` should decrement the counter variable and return it.
    let count = 0
    return {
        increment: () => {
            return (count = count + 1)
        },
        decrement: () => {
            return (count = count - 1)
        },
    }
}

module.exports = counterFactory

function limitFunctionCallCount(cb, n) {
    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned

    if (cb == undefined || n == undefined) {
        throw new Error("partial or no argument")
    } else {
        let executionCount = 0
        function invoke_cb(...args) {
            executionCount = executionCount + 1
            if (executionCount <= n) {
                return cb(...args)
            } else {
                return null
            }
        }
        return invoke_cb
    }
}

module.exports = limitFunctionCallCount

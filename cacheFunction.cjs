function cacheFunction(cb) {
    // Should return a function that invokes `cb`.
    // A cache (object) should be kept in closure scope.
    // The cache should keep track of all arguments have been used to invoke this function.
    // If the returned function is invoked with arguments that it has already seen
    // then it should return the cached result and not invoke `cb` again.
    // `cb` should only ever be invoked once for a given set of arguments.
    if (cb == undefined) {
        throw Error("partial or no arguments passed")
    } else {
        let cache = {}

        function invoke_cb(...args) {
            // console.log(args)
            let argsString = JSON.stringify(args)
            // console.log(argsString)
            if (argsString in cache) {
                // console.log("from cache")
                return cache[argsString]
            } else {
                // console.log("adding to cache")
                let result_from_cb = cb(...args)
                cache[argsString] = result_from_cb
                // console.log(cache)
                return cache[argsString]
                // return cb.apply(null,args)
            }
        }

        return invoke_cb
    }
}

module.exports = cacheFunction
